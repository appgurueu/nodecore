#### Join the Community:

- Discord: <https://discord.gg/NNYeF6f>
- Matrix: [#nodecore:matrix.org](https://matrix.to/#/#nodecore:matrix.org)
- IRC: **#nodecore** on [irc.libera.chat](https://libera.chat/)
- Semi-Unofficial Wiki: [nodecore.mine.nu](https://nodecore.mine.nu/)

#### Support the Project

- Donate: <https://liberapay.com/NodeCore>

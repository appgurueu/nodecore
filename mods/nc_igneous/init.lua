-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("amalgam")
include("pumice")
include("harden")
include("pumblob")
include("hints")
include("renew")

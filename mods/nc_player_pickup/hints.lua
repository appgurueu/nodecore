-- LUALOCALS < ---------------------------------------------------------
local nodecore
    = nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.register_hint(
	"drop an item",
	"item_drop"
)

nodecore.register_hint(
	"drop all your items at once",
	"aux_item_drop",
	"item_drop"
)

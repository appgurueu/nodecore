-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("util")
include("api")
include("falling_ent")
include("item_ent")
include("item_merge")
include("fallthrufix")

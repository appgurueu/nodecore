**A surreal dream-world where everything is possible but nothing is easy.**  Peaceful and Zen.  Brutally obstinate.  *Prepare to unlearn everything you thought you knew about voxel sandbox games.*

#### Features:

- A complete game, deep and complex, with mods available but not required.
- An immersive 3D world, with no pop-ups and minimal HUDs between you and the world.
- Paragon of the "voxel" concept, with virtually every in-world thing on the voxel grid.
- In-world crafting systems with unique recipes.
- Rich emergent mechanics, customizable machines built of discrete and fungible parts.
- Complex in-game technology: optical logic circuits, hinged/rotary machinery, automated crafting.
- Subtle dynamics: material angle of repose, fuel and soil quality, conservation and decay.
- Exercise critical thinking and logical reasoning, and learn through experimentation.
- Built-in new player guide, hint system providing low-spoiler guidance.
- Playable and consistent across all platforms, single or multi-player.  Mobile-friendly, gamepad-friendly.
- Eggcorns!  Pumwater!

#### Warnings:

- **NodeCore is a *different kind* of difficult.**  Advancement is tied to innovation and discovery, rather than honing skills, and is likely difficult, complex, and unforgiving.  If you want to unlock most of the content, expect it to require both creativity and patience.
- Only *very* basic, minimal-spoiler guidance is included in the game.  Figuring things out yourself is part of the default experience.  If you need more guidance, **Join the Community** (below).
- Do not expect intuition you have built up from other games, including other voxel sandbox or puzzle games, to apply here.  This game has its own logic and learning it is part of the full experience.

#### Recent Breaking Changes:

N.B. this is only for changes that remove some old way of doing something (i.e. not new features or new alternative routes) and are old enough that many players are expected to rely on them.  It also does not include most bugfixes.

- `01683215-4c09d412`: Recipes for rakes have been changed based on the "intuitive" shapes new players were observed trying.  It uses one fewer adze than the old recipe, and is overall less expensive.
- `01671185-4304c47c`: Recipes for storage boxes (shelves, cases/tanks, and crates) have changed.  Instead of 3x3 recipes, they are based on placing things into forms, and forms are made from frames.  This includes a change in the overall cost of storage units.
- `01643785-c34bc395`: Leaching soils now requires raked soils.  It now requires much more human intervention than before.

#### Editions:

========================================================================
ISSUES-GAME: Gameplay-affecting issues
------------------------------------------------------------------------

  ####   #####    ####      #    #       ######  #####    ####
 #       #    #  #    #     #    #       #       #    #  #
  ####   #    #  #    #     #    #       #####   #    #   ####
      #  #####   #    #     #    #       #       #####        #
 #    #  #       #    #     #    #       #       #   #   #    #
  ####   #        ####      #    ######  ######  #    #   ####

- Move some ABMs into soaking API:
	- Sedges growing
	- Lava melting stone
	- Hardening/softening stone

- Move sedge growing abm into soaking.

- Continuing issues with storebox fall-thru
	- Items now refusing to fall from a form into a crate or
	  case directly below.
		- This is a side effect of logic to prevent items from falling
		  OUT the bottom of lode crates that are suspended above air
		  and will not be simple to fix.
	- Prills dropping out of glowing lode spawn inside a
	  storebox below, but this allows them to fall through
	  the bottom (since they're already past colliding with
	  the box and being absorbed)
		- Maybe entities need to try to settle into a box
		  if it WAS in the box's node space last tick but
		  is below it now, and the box isn't open on
		  the bottom.

- Skip more entity<->entity<->node transition steps
	- falling_node / item_entity conversions
	- Instant settling on catapult for items ejected with
	  downward component into a settlable space

- Lantern brightness non-linearity
	- level = ceil(4 * (1 + 2*arcsin(charge/50 - 1)/pi))
	  (thanks to Oblomov)

- Rake recipe is a major problem
	- Lots of players are getting caught up on it
	- Replace recipe:
		- Add "partial" rakes for 1, 2, and 3 tines.
		- Using adze on handle makes partial rake.
		- Using adze on partial rake upgrades it.
		- Once a full rake is completed it works as normal.
		- Partial rakes have rake functionality but break
		  after a single use.
			- Break into appropriate prills (lode)
	- Hide rake recipe until later in the game?
		- Maybe add a generic "# hints completed" qualifier?
		- Maybe add a number of hours played (non-idle) qualifier?

- All flora should be radiation-sensitive.
	- Currently only flowers, add sedges and rushes
	- Should be at risk of harm during growth or spreading
		- Rushes may dry out
		- Sedges may be stunted, lose growth stages

- Door placement convenience
	- If player places a door panel against the face of another
	  door panel (sneak+place) then match axis if possible.

- Cheats
	- Should these be spun off into a separate mod?
		- Maybe a separate mod within the game so they
		  are still standard and don't require a separate
		  installation.
	- Rename the admin hand tool again.
	- Remove fullbright from admin hand.
	- Separate command to set a minimum player dynamic light.
	- Priv to disable skybox/sunlight diminishing by depth?
	- Add radius to /snuff, /ignite, /growtrees, etc
	- Add a /soak cheat to push soaking?
	- Add a /forest or /grove cheat to sprinkle trees around
		- Like the flora redecorate
		- Useful for servers with spawn tree attrition
	- /snuff should probably snuff torches too
		- General snuffable/on_snuff API?
		- Merge with growtrees for a general on_cheat API?

- Emergency digging mechanic issues
	- Players still confused by it.

- Detect and warn players about misuse of singlenode mapgen
	- Mods can register if they're supporting singlenode.
	- We can detect player "falling forever" by scanning down
	  to find anything other than air before ignore.

- Re-examine chiseling recipes
	- Bricks accept annealed chisels, which makes some sense
	  from a game balance perspective (cosmetic-only = cheap)
	  but less re: realism.
	- Similarly doors all require tempered chisels, and maybe
	  at least wooden doors should accept annealed.

- Cannot catapult an item into a form from below.
	- Is this actually important?

- Thatch and wicker should probably have recycling recipes.

- Do we really need stack_as_node for leaves, sticks, and a
  few other ordinary items anymore, or has our falling_node
  rework made that irrelevant now?

- Sponge Fixes
	- Fix crappy sponge death mechanic
		- Remove special treatment of glass case and glass nodes
			- May need to find a new use-case for glass case?
	- Improve sponge colony growth shape
		- Prevent tall vertical column growth seen on NCC by Kimapr
		- Instead of just pushing out a sponge, floodfill to find colony
		(reuse colony volume check), then pick a random surface node
		to grow from; maybe save candidate water spots during the flood
		scan and grow non-locally to that?

- Hint system:
	- Split hint descriptions into "pre" and "post" parts.
		- Undiscovered, show just "pre" part (1 stick + 4 adze)
		- After, show both (1 stick + 4 adze => rake)
	- Make all crafts/recipes trigger witnessing?
	- Hide stylus hint until after mixed concrete

- Make a way to prevent crafting on place
	- Sneak? Aux?
	- Confirmation UI after completing recipe, e.g. floating ent
	- Make all recipes include some pummel factor?

- Door press recipe limitations
	- Node-placing recipes don't work with itemstacks
		- Tool and torch making don't work
		- We should be able to detect failed-catapulting for this
	- Stackapply recipes
		- Needed for automatic eggcorn planting

- Health system slot bias should be a bit stronger

- Consider making offline (e.g. soaking) growth rates sublinear
  with time, e.g. diminishing returns for offline time, to create
  incentives to build nearby

- Stone softening rates feel off.
	- Once first stage of stone softens, others follow really
	  quickly (exponential runaway basically).
	- If you leave stone soaking for a while, it looks like it
	  basically softens all or nothing.
	- Can we smooth this out and make it less jarring?

- Item ent fixes
	- Make crush damage use velocity relative to player,
	  not absolute velocity, to calculate damage

- A way to visualize the infusion rate effects would be nice
	- Particles don't work due to semitransparent liquid

- Add a way to extend wandering generations
	- For glass, heat sources
	- For concrete, moisture sources

- Rakes should probably not charge durability for digging first item
  stack, or should charge reduced wear for item stacks.

- Tarstone should produce coal particles consistently when broken back
  down to regular cobble, but it only works with the base node, not the
  etched ones.

- Valleys mapgen rivers seem to rely on having an actual separate
  river water registration with a specific liquid range.

- Door-wheels do not affect attached glyphs...

- Consider changing wet/dry stack splitting
	- Currently, original stack remains in place, wetted/dried
	  stack jumps out to new spot.
	- Instead, maybe wetted/dried stays in place and original
	  jumps out (unless there's an adjacent wetted/dried stack to
	  reuse).
	- More similar to the way flammable stack ignition works
	- Just meaner overall

- Consistentize soaking/offline mechanics
	- Use a DNT to make check regular for things actually soaking
	- To consider:
		- Tree growth
		- Compost
		- Sponge expiration
		- Torch expiration
		- Leaching
		- Repacking
		- Concrete
		- Lux renew
	- Logic: Action by a living thing (i.e. presence of player or
	  microorganisms) is what keeps time ticking...?
	- Be more consistent with some non-offline mechanics like
	  fire fuel consumption
	- When we did torches via expiration metadata, the reason at the
	  time was that there was no way to get them to tick reliably like
	  we can with nodes; with AISMs this is no longer the case.

- Repose changes
	- Staves/rods should repose a lot less or not at all.
	- Rework repose to be based on amount of loose material below, not
	  amount of distance available to fall?  repose only needs to have a
	  particular angle as measured on flat land?

- Tote issues:
	- Consider updating tote recipe.
	- Totes should do face-connected search for totables; don't pick up
	  shelves only touching via a corner.

- Flammables should respond via AISM when bathed in fire or igniting
  liquids like molten glass or rock.

- Should there be pockets of softer stone at depth, like there is gravel?
	- Register ores for softer stone of each level
	- Maybe some ores for cobble and/or loose cobble?
	- Maybe ores for dirt, sand?

- Consider checking for a nearby active player before running
  "fast-evolving" ABMs that don't normally work offline, like fire
  extinguishing/spreading?
	- This would make gameplay more fair for those who tend to
	  go AFK suddenly and can't return for a long time.
	- This would reduce the gap between things that support the offline
	  mechanic and those that don't between MP and SP.

- Make separate walkable/non-walkable stack nodes.
	- Should sticks and eggcorns be non-walkable?

........................................................................
========================================================================

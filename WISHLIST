========================================================================
           -----===== PUBLIC CONTRIBUTION WISHLIST =====-----
------------------------------------------------------------------------
Remember, check out the CONTRIBUTING file for guidelines on contributing
to the project!

- Testing and bug reports!

- Translations:
	- https://nodecore.mine.nu/trans/projects/nodecore/core/

- Help (PRs and/or advocacy) with Minetest engine issues:
	- Major game-breakers:
		- #11531: Prevents some effects from being usable, especially
		  a fancy skybox with extended navigation capabilities.
		- #10304: Causes looktips to conflict with pickpocketing, so
		  pickpocketing, being the lower priority feature, got cut;
		  fixing this could feasibly allow both to coexist.
	- Minor game-breakers:
		- #9377: This causes some annoyances when handling lux tools,
		  and makes a certain mechanic blatant when it's intended to be
		  subtle.
		- #7876: Causes a damage flash when a new player joins, or a
		  player from before 2020-02-19 when max HP was changed.
		- #8952: This causes hidden bases and other items to be visible
		  through walls when ent/player transfer distance is high; when
		  low, instead it causes players/ents to be invisible even when
		  terrain is loaded and visible, causing distant player activity
		  to look like poltergeists.
	- Blocking new features:
		- #11404: Desktop/mobile controls lack parity, so certain input
		  combinations (like sneak+punch) cannot be used for things that
		  are gameplay-essential.  This limits input expressivity and
		  requires awkward inputs to work around, and is a minefield for
		  potential discrimination problems due to the difficulty of
		  ensuring that all future input design takes this into account.
	- Internal/annoyance:
		- #9934: This bug is causing us to maintain a workaround in
		  NodeCore which is fragile and keeps breaking, leading to
		  significant network load for some clients.
		- #6963: If this is implemented, we may have a more efficient
		  way to handle "visinv" nodes, i.e. stacks, storage boxes,
		  and other container nodes.
		- #12317: Not having consistent and known player movement
		  interpolation behavior makes it hard to keep player
		  nametags matched with the average player position instead
		  of jumping ahead or lagging behind.

- Help with documentation
	- Improvements to the in-game guidance, esp. for onboarding new
	  players, are wanted.  Ideally we want to offer a spoiler-free
	  experience by default, but want players to have options/resources
	  in the event they can't figure something out.
	- Advising players where they can find external resources, or other
	  sources of help, such as wikis, youtube, chatrooms, multiplayer
	  servers, etc.
	- Work on improving the nodecore wiki itself.

- Improved versions of any art assets (sounds, textures, models).
	- All textures for base inclusion should be 16x (keep
	  high-def textures to texture packs).  Note that textures
	  larger than 16x are allowed (and already used) in the game
	  for textures that "spill over" into neighoring spaces, but
	  the spatial resolution of 16 pixels per node is maintained.
	- It must be practical to maintain a consistent style for
	  other assets, including future related ones, or the content
	  being reworked should be mature and stable, independent of
	  other content areas, and the new textures should mesh
	  reasonably well with the existing style.
	- Alternatively, publish your mods, texturepacks, or soundpacks
	  directly to the community, via ContentDB.
	- Replacements for nodeboxes and meshes that render faster (i.e.
	  better FPS preservation when tons of them are around) on most
	  or all systems are wanted.

- Publicity and player content:
	- Help spread NodeCore!  Share your experiences, screenshots,
	  videos and streams!
	- NodeCore especially needs a "trailer" video.
		- https://nodecore.mine.nu/w/Teaser/Trailer
	- Let the developers know if you have shared something about
	  NodeCore, or would like guidance creating something to share.

........................................................................
========================================================================
